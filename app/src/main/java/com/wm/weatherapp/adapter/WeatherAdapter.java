package com.wm.weatherapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.bumptech.glide.Glide;
import com.wm.weatherapp.R;
import com.wm.weatherapp.model.ListItem;
import com.wm.weatherapp.utils.CommonMethod;
import com.wm.weatherapp.utils.DatesHelper;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.WeatherItemHolder> {

    private Context context;
    private LayoutInflater inflater;
    ArrayList<ListItem> data;
    public WeatherAdapter(Context context, ArrayList<ListItem> data) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
    }

    @Override
    public WeatherItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.weather_items, parent, false);
        return new WeatherItemHolder(view);
    }


    @Override
    public void onBindViewHolder(@NotNull WeatherItemHolder holder, int position) {
        ListItem current = data.get(position);
        if (DatesHelper.isToday(current.getDtTxt()))
        {
            holder.weatherLayout.setBackground(context.getResources().getDrawable(R.drawable.background_day_1));
        }
        else {
            holder.weatherLayout.setBackground(context.getResources().getDrawable(R.drawable.background_day_2));
        }

        Date date = DatesHelper.getDate(current.getDtTxt());
        SimpleDateFormat format = new SimpleDateFormat("dd MMMM yyyy");
        String displayDate = format.format(date);

        SimpleDateFormat outFormat = new SimpleDateFormat("EEEE");
        String goal = outFormat.format(date);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");
        String dateTime = simpleDateFormat.format(date.getTime());
        holder.textDateTime.setText(goal + ", "+dateTime);
        holder.textMain.setText(current.getWeather().get(0).getMain());
        holder.textDescription.setText((int) CommonMethod.f2c(Float.parseFloat(current.getMain().getTemp().toString())) + "°C" );

        Glide.with(context).load("http://openweathermap.org/img/w/" + current.getWeather().get(0).getIcon() + ".png")
                .placeholder(R.mipmap.ic_launcher)
                .error(R.mipmap.ic_launcher)
                .into(holder.imageIcon);
    }
    @Override
    public int getItemCount() {
        return data.size();
    }

    public class WeatherItemHolder extends RecyclerView.ViewHolder {
        private LinearLayout weatherLayout;
        private TextView textDateTime;
        private TextView textDescription;
        private ImageView imageIcon;
        private TextView textMain;

        public WeatherItemHolder(@NonNull View itemView) {
            super(itemView);
            weatherLayout = itemView.findViewById(R.id.weatherLayout);
            textDateTime = itemView.findViewById(R.id.textDateTime);
            textMain = itemView.findViewById(R.id.textViewMain);
            textDescription = itemView.findViewById(R.id.textViewDescription);
            imageIcon = itemView.findViewById(R.id.imageView1);
        }
    }
}

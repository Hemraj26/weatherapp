package com.wm.weatherapp.session

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import com.wm.weatherapp.session.SessionVar.KEY_ACCESS_TOKEN

import java.util.*

object SessionVar {
    const val KEY_ACCESS_TOKEN = "key_access_token"
}

private const val PREF_NAME = "weatherapp"

class SessionManager(
        context: Context
) {

    private val appContext = context.applicationContext

    private val preference: SharedPreferences =
            appContext.getSharedPreferences(PREF_NAME, 0)   //0 for private mode

    private val editor: SharedPreferences.Editor = preference.edit()


    fun createTokenSession(token: String) {
        editor.putString(KEY_ACCESS_TOKEN, token).apply()
    }

    fun getAccessToken(): HashMap<String, String> {
        val user = HashMap<String, String>()
        user.put(KEY_ACCESS_TOKEN, preference.getString(KEY_ACCESS_TOKEN, null).toString())
        return user
    }

    fun logoutUser() {
        editor.clear()
        editor.apply()
    }


}
package com.wm.weatherapp

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.wm.weatherapp.session.SessionManager
import com.wm.weatherapp.session.SessionVar
import com.wm.weatherapp.ui.HomeActivity
import com.wm.weatherapp.ui.LoginActivity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
class SplashActivity : AppCompatActivity() {
    private lateinit var accessToken: String
    private lateinit var sessionManager: SessionManager
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
         sessionManager = SessionManager(this)
         accessToken = sessionManager.getAccessToken().get(SessionVar.KEY_ACCESS_TOKEN).toString()

        GlobalScope.launch(Dispatchers.Main) {
            delay(2000)
            if (accessToken.isEmpty() || accessToken.equals("null"))
            {
                startActivity(Intent(this@SplashActivity, LoginActivity::class.java))
                finish()
            }
            else
            {
                startActivity(Intent(this@SplashActivity, HomeActivity::class.java))
                finish()
            }
        }
    }
}
package com.wm.weatherapp.utils;

import androidx.annotation.Nullable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DatesHelper {
    @Nullable
    public static Date getDate(String dt)
    {
        try
        {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date date = format.parse(dt);
            return date;
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }

        return null;
    }


    public static boolean isToday(String dt)
    {
        try
        {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date weatherdate = format.parse(dt);

            Date todayWithNoTime = format.parse(format.format(new Date()));
            return weatherdate.compareTo(todayWithNoTime) == 0;
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }

        return false;
    }
}

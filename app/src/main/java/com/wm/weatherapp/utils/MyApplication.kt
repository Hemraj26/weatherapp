package com.wm.weatherapp.utils

import android.app.Application
import com.wm.weatherapp.network.NetworkConnectionInterceptor
import com.wm.weatherapp.network.RemoveCharacterInterceptor
import com.wm.weatherapp.network.RestApi
import com.wm.weatherapp.repositiory.WeatherRespository
import com.wm.weatherapp.viewmodel.WeatherViewModelFactory

import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class MyApplication : Application(), KodeinAware {
    override val kodein = Kodein.lazy {
        import(androidXModule(this@MyApplication))

        bind() from singleton { NetworkConnectionInterceptor(instance()) }
        bind() from singleton { RemoveCharacterInterceptor() }
        bind() from singleton { RestApi(instance(),instance()) }
        bind() from singleton { WeatherRespository(instance()) }
        bind() from provider { WeatherViewModelFactory(instance()) }
    }
}
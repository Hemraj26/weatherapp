package com.wm.weatherapp.utils;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.wm.weatherapp.R;


public class ProgressD extends Dialog {

    private ProgressD(Context context, int theme) {
        super(context, theme);
    }
    public static ProgressD show(Context context, CharSequence message, boolean indeterminate,
                                 boolean cancelable, OnCancelListener cancelListener)
    {
        ProgressD dialog = new ProgressD(context, R.style.ProgressD);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setTitle("");
        dialog.setContentView(R.layout.progress_hud_bg);
        if (message == null || message.length() == 0)
        {
            dialog.findViewById(R.id.message).setVisibility(View.GONE);
        }
        else
            {
        }
        dialog.setCancelable(cancelable);
        dialog.setOnCancelListener(cancelListener);

        dialog.show();
        Window window = dialog.getWindow();
        assert window != null;
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        return dialog;
    }
}

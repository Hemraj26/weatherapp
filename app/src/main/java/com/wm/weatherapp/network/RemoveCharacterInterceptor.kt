package com.wm.weatherapp.network

import okhttp3.Interceptor
import okhttp3.Response

class RemoveCharacterInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val path = request.url.toString()

        val string1 = path.replace("%26", "&") // replace
        val string2 = string1.replace("%3D", "=") // replace

        val newRequest = request.newBuilder()
                .url(string2)
                .build()

        return chain.proceed(newRequest)
    }

}
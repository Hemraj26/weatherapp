package com.wm.weatherapp.network

import android.util.Log
import com.wm.weatherapp.BuildConfig
import com.wm.weatherapp.utils.Constants
import com.wm.weatherapp.model.WeatherResponse
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import java.util.concurrent.TimeUnit


interface RestApi
{
    @GET("forecast")
    suspend fun getWeather(@Query("q") city: String?): Response<WeatherResponse>
    companion object {
        operator fun invoke(
            networkConnectionInterceptor: NetworkConnectionInterceptor,
            intecepterREm: RemoveCharacterInterceptor
        ): RestApi {
            val okkHttpclient = OkHttpClient.Builder()
                .addInterceptor(intecepterREm)
                //.addInterceptor(networkConnectionInterceptor)
                .addInterceptor(HttpLoggingInterceptor { message ->
                    if (BuildConfig.DEBUG)
                        Log.e("Logger", "response " + message)
                }.apply {
                    level = HttpLoggingInterceptor.Level.BODY
                })
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build()

            return Retrofit.Builder()
                .client(okkHttpclient)
                .baseUrl(Constants.baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(RestApi::class.java)
        }
    }
}
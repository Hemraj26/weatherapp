package com.wm.weatherapp.viewmodel

import androidx.lifecycle.ViewModel
import com.wm.weatherapp.repositiory.WeatherRespository

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class WeatherViewModel(
    private val respository: WeatherRespository
) : ViewModel() {

    suspend fun getWeatherData(city: String) = withContext(Dispatchers.IO) {
        respository.getWeatherapi(city)
    }
}
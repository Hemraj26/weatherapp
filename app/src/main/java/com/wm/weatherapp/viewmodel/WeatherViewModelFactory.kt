package com.wm.weatherapp.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.wm.weatherapp.repositiory.WeatherRespository


@Suppress("UNCHECKED_CAST")
class WeatherViewModelFactory(
    private val repository: WeatherRespository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return WeatherViewModel(repository) as T
    }
}
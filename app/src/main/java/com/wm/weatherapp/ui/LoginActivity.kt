package com.wm.weatherapp.ui

import android.Manifest
import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthOptions
import com.google.firebase.auth.PhoneAuthProvider
import com.google.firebase.auth.PhoneAuthProvider.ForceResendingToken
import com.google.firebase.auth.PhoneAuthProvider.OnVerificationStateChangedCallbacks
import com.wm.weatherapp.R
import com.wm.weatherapp.databinding.ActivityLoginBinding
import com.wm.weatherapp.utils.ProgressD
import kotlinx.android.synthetic.main.activity_login.*
import java.util.concurrent.TimeUnit


class LoginActivity : AppCompatActivity()
{
    private var code: String?=null
    private var progressDialog: ProgressD? = null
    private lateinit var binding: ActivityLoginBinding
    private var mAuth: FirebaseAuth? = null
    private var verificationId: String? = null
    private val PERMISSION_REQ_CODE = 1 shl 2
    private val PERMISSIONS = arrayOf(
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION)
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        init()
        clickEvents()
    }

    private fun init()
    {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        mAuth = FirebaseAuth.getInstance()
    }

    private fun sendVerificationCode(number: String)
    {
        progressDialog = ProgressD.show(
            this@LoginActivity,
            resources.getString(R.string.login),
            true,
            false,
            null
        )

        val options = PhoneAuthOptions.newBuilder(mAuth!!)
                .setPhoneNumber(number)
                .setTimeout(60L, TimeUnit.SECONDS)
                .setActivity(this)
                .setCallbacks(mCallBack)
                .build()
        PhoneAuthProvider.verifyPhoneNumber(options)
    }
    private val mCallBack: OnVerificationStateChangedCallbacks = object : OnVerificationStateChangedCallbacks()
    {
        override fun onCodeSent(s: String, forceResendingToken: ForceResendingToken)
        {
            super.onCodeSent(s, forceResendingToken)
            verificationId = s
            progressDialog?.dismiss()
            Toast.makeText(this@LoginActivity, getString(R.string.otpsend), Toast.LENGTH_LONG).show()

           val intent = Intent(this@LoginActivity, OtpActivity::class.java)
            intent.putExtra("smscode",code)
            intent.putExtra("verificationid",verificationId)
            startActivity(intent)
        }
        override fun onVerificationCompleted(phoneAuthCredential: PhoneAuthCredential)
        {
            code = phoneAuthCredential.smsCode
            if (code != null)
            {
                progressDialog?.dismiss()
            }
        }
        override fun onVerificationFailed(e: FirebaseException)
        {
            Toast.makeText(this@LoginActivity, e.message, Toast.LENGTH_LONG).show()
            progressDialog?.dismiss()
        }
    }



    private fun clickEvents()
    {
          btnlogin.setOnClickListener {
              if(binding.etNumber.text.toString().isEmpty()|| binding.etNumber.text.toString().equals(
                      ""))
              {
                  Toast.makeText(
                      this@LoginActivity,
                      getString(R.string.enter_valid_otp),
                      Toast.LENGTH_SHORT
                  ).show()
              }
              else
              {
                  checkPermission()
              }
          }
    }

    private fun checkPermission() {
        var granted = true
        for (per in PERMISSIONS) {
            if (!permissionGranted(per)) {
                granted = false
                break
            }
        }
        if (granted)
        {
            checkGPS()
        }
        else {
            requestPermissions()
        }
    }

    private fun checkGPS() {
        val manager = this.getSystemService(LOCATION_SERVICE) as LocationManager
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Toast.makeText(
                this@LoginActivity,
                "Please Turn GPS on to continue..",
                Toast.LENGTH_LONG
            ).show()
        }
        else
        {
            sendVerificationCode("+91" + etNumber.text.toString().trim());
        }
    }

    private fun permissionGranted(permission: String): Boolean {
        return ContextCompat.checkSelfPermission(
            this, permission
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            this,
            PERMISSIONS,
            this.PERMISSION_REQ_CODE
        )
    }

    override fun onBackPressed() {
        finish()
        finishAffinity()
    }
}
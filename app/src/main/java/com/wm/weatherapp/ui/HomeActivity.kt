package com.wm.weatherapp.ui

import android.Manifest
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthProvider
import com.wm.weatherapp.R
import com.wm.weatherapp.adapter.WeatherAdapter
import com.wm.weatherapp.databinding.ActivityHomeBinding
import com.wm.weatherapp.model.ListItem
import com.wm.weatherapp.session.SessionManager
import com.wm.weatherapp.utils.*
import com.wm.weatherapp.viewmodel.WeatherViewModel
import com.wm.weatherapp.viewmodel.WeatherViewModelFactory
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_home.view.*
import kotlinx.android.synthetic.main.activity_login.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.util.*


@Suppress("DEPRECATION")
class HomeActivity : AppCompatActivity(), OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener, LocationListener, KodeinAware
{
    private lateinit var sessionManager: SessionManager
    var city: String?=null
    var weatherAdapter:WeatherAdapter? = null
    var mapFragment: SupportMapFragment? = null
    var mMarkerPoints: ArrayList<LatLng>? = null
    private var mMap: GoogleMap? = null
    lateinit var mGoogleApiClient: GoogleApiClient
    var mLastLocation: Location? = null
    private var current_location_longitude = 0.0
    private var current_location_latitude = 0.0
    var mLocationRequest: LocationRequest? = null
    private var latLng_origin: LatLng? = null
    lateinit var mCurrLocationMarker: Marker
    lateinit var binding: ActivityHomeBinding
    private lateinit var viewModel: WeatherViewModel
    private val factory: WeatherViewModelFactory by instance()
    override val kodein by kodein()
    private var progressDialog: ProgressD? = null
    private var mAuth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        init()
        clickEvents()
    }
    private fun init()
    {
        sessionManager = SessionManager(this)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home)
        mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment!!.getMapAsync(this)
        mMarkerPoints = ArrayList()
        viewModel = ViewModelProvider(this, factory).get(WeatherViewModel::class.java)
    }

    override fun onMapReady(p0: GoogleMap?) {
        mMap = p0
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            if (ContextCompat.checkSelfPermission(
                    this@HomeActivity,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
                == PackageManager.PERMISSION_GRANTED
            ) {
                //Location Permission already granted
                buildGoogleApiClient()
                mMap!!.isMyLocationEnabled = true
            } else
            { }
        }
        else {
            buildGoogleApiClient()
            mMap!!.isMyLocationEnabled = true
        }
    }

    private fun clickEvents()
    {
        btnlogout.setOnClickListener {
            AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(getString(R.string.really_exit))
                .setMessage(getString(R.string.are_sure_logout))
                .setPositiveButton(getString(R.string.yes)) {
                        dialog: DialogInterface?, which: Int ->
                    val intent = Intent(this@HomeActivity, LoginActivity::class.java)
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                    startActivity(intent)
                    sessionManager.logoutUser()
                }
                .setNegativeButton(getString(R.string.no), null).show()
        }
    }

    @Synchronized
    protected fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(this@HomeActivity)
            .addConnectionCallbacks(this@HomeActivity)
            .addOnConnectionFailedListener(this@HomeActivity)
            .addApi(LocationServices.API)
            .build()
            mGoogleApiClient.connect()
    }

    override fun onConnectionFailed(p0: ConnectionResult)
    {

    }

    override fun onConnected(p0: Bundle?)
    {
        mLocationRequest = LocationRequest()
        mLocationRequest!!.interval = 1000000
        mLocationRequest!!.fastestInterval = 1000000
        mLocationRequest!!.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
        if (ContextCompat.checkSelfPermission(
                this@HomeActivity,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient,
                mLocationRequest!!,
                this
            )
        }
    }

    override fun onConnectionSuspended(p0: Int) {
        TODO("Not yet implemented")
    }


    override fun onPause() {
        super.onPause()
        if (mGoogleApiClient != null) {
            try {
                LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient,
                    this as LocationListener
                )
            } catch (e: Exception) {
                // Toast.makeText(getActivity(), "Try Again", Toast.LENGTH_SHORT).show();
            }
        }
    }

    override fun onResume() {
        super.onResume()
        mapFragment!!.onResume()
    }

    override fun onLocationChanged(location: Location?)
    {
        try {
            mLastLocation = location
            if (location != null) {
                current_location_latitude = location.getLatitude()
            }
            if (location != null) {
                current_location_longitude = location.getLongitude()
            }
            if (location != null) {
                latLng_origin = LatLng(location.getLatitude(), location.getLongitude())
            }
            mMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng_origin, 15f))
            if (this::mCurrLocationMarker.isInitialized)
            {
                mCurrLocationMarker!!.remove()
            }
            else
            {
                val markerOption1 = MarkerOptions()
                latLng_origin?.let { markerOption1.position(it) }
                markerOption1.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
                mCurrLocationMarker = mMap!!.addMarker(markerOption1)
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }

        val gcd = Geocoder(this, Locale.getDefault())
        val addresses: List<Address> = gcd.getFromLocation(
            current_location_latitude,
            current_location_longitude,
            1
        )
        if (addresses.size > 0)
        {
             city = addresses[0].getLocality()
            if(CommonMethod.isOnline(this@HomeActivity))
            {
                getWeatherData(city.toString() + "&appid=6980675659d56e39c2621b8a57de4d39")     //weather api call
            //    Log.d("listweather", city)
            }
            else
            {
                CommonMethod.showToast(getString(R.string.check_internet), this)
            }
        }
        else
        {}
    }

    private fun getWeatherData(city1: String)
    {
        progressDialog = ProgressD.show(
            this@HomeActivity,
            resources.getString(R.string.login),
            true,
            false,
            null
        )
        Coroutines.runOnIO {
            try {
                val weatherResponse = viewModel.getWeatherData(city1)
                Coroutines.runOnMain {
                    progressDialog?.dismiss()
                   // Log.d("listweather", weatherResponse.list?.get(0)?.main?.temp.toString())
                    val mLayoutManager = LinearLayoutManager(
                        this@HomeActivity, RecyclerView.HORIZONTAL, false
                    )
                    recyclerViewForWeather.setLayoutManager(mLayoutManager)
                    weatherAdapter = WeatherAdapter(
                        this@HomeActivity,
                        weatherResponse.list as ArrayList<ListItem>?
                    )
                    recyclerViewForWeather.setAdapter(weatherAdapter)
                    cityname.setText(" " + ":" + " " + city)
                }

            } catch (e: ApiException)
            {
                e.printStackTrace()
                Coroutines.runOnMain {
                    progressDialog?.dismiss()
                }
            } catch (e: NoInternetException)
            {
                e.printStackTrace()
                 Coroutines.runOnMain {
                    progressDialog?.dismiss()
                }
            }
        }
    }

    override fun onBackPressed()
    {
          AlertDialog.Builder(this)
            .setIcon(android.R.drawable.ic_dialog_alert)
            .setTitle(getString(R.string.really_exit))
            .setMessage(getString(R.string.are_sure_exit))
            .setPositiveButton(getString(R.string.yes))
            { dialog: DialogInterface?, which: Int -> this@HomeActivity.finishAffinity() }
            .setNegativeButton(getString(R.string.no), null).show()
    }
}



package com.wm.weatherapp.ui

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import com.wm.weatherapp.R
import com.wm.weatherapp.databinding.ActivityOtpBinding
import com.wm.weatherapp.session.SessionManager
import com.wm.weatherapp.utils.ProgressD
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_otp.*


class OtpActivity : AppCompatActivity()
{
    private var finalOtp: String?=null
    lateinit var binding: ActivityOtpBinding
    private lateinit var etOtp1: EditText
    private lateinit var etOtp2: EditText
    private lateinit var etOtp3: EditText
    private lateinit var etOtp4: EditText
    private lateinit var etOtp5: EditText
    private lateinit var etOtp6: EditText
    private var code: String?=null
    private var verificationid: String?=null
    private var mAuth: FirebaseAuth? = null
    private var progressDialog: ProgressD? = null
    private lateinit var sessionManager: SessionManager
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        init()
        clickEvents()
    }

    private fun init()
    {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_otp)
        etOtp1 = findViewById(R.id.etOtp1)
        etOtp2 = findViewById(R.id.etOtp2)
        etOtp3 = findViewById(R.id.etOtp3)
        etOtp4 = findViewById(R.id.etOtp4)
        etOtp5 = findViewById(R.id.etOtp5)
        etOtp6 = findViewById(R.id.etOtp6)
        verificationid = intent.getStringExtra("verificationid")
        mAuth = FirebaseAuth.getInstance()
        sessionManager = SessionManager(this)
    }

    private fun clickEvents()
    {
        btnverifiy.setOnClickListener {
            val otp1 = etOtp1.text.toString().trim()
            val otp2 = etOtp2.text.toString().trim()
            val otp3 = etOtp3.text.toString().trim()
            val otp4 = etOtp4.text.toString().trim()
            val otp5 = etOtp5.text.toString().trim()
            val otp6 = etOtp6.text.toString().trim()
            finalOtp = otp1.plus(otp2).plus(otp3).plus(otp4).plus(otp5).plus(otp6)
            if(otp1.isEmpty() || otp2.isEmpty() || otp3.isEmpty() || otp4.isEmpty()
                || otp5.isEmpty()|| otp6.isEmpty())
            {
                Toast.makeText(
                    this@OtpActivity,
                    getString(R.string.enter_valid_otp),
                    Toast.LENGTH_SHORT
                ).show()
            }
            else
            {
               verifyOtp()
            }
        }

        etOtp1.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun afterTextChanged(s: Editable) {
                val text: String = s.toString().trim()
                if (text.length == 1)
                    etOtp2.requestFocus()
            }
        })
        etOtp2.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun afterTextChanged(s: Editable) {
                val text: String = s.toString().trim()
                if (text.length == 1)
                    etOtp3.requestFocus()
                else if (text.length == 0)
                    etOtp1.requestFocus()
            }
        })
        etOtp3.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun afterTextChanged(s: Editable) {
                val text: String = s.toString().trim()
                if (text.length == 1)
                    etOtp4.requestFocus()
                else if (text.length == 0)
                    etOtp2.requestFocus()
            }
        })
        etOtp4.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun afterTextChanged(s: Editable) {
                val text: String = s.toString().trim()
                if (text.length == 1)
                    etOtp5.requestFocus()
                else if (text.length == 0)
                    etOtp3.requestFocus()
            }
        })

        etOtp5.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun afterTextChanged(s: Editable) {
                val text: String = s.toString().trim()
                if (text.length == 1)
                    etOtp6.requestFocus()
                else if (text.length == 0)
                    etOtp4.requestFocus()
            }
        })

        etOtp6.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun afterTextChanged(s: Editable) {
                val text: String = s.toString().trim()
                if (text.length == 0)
                    etOtp5.requestFocus()
            }
        })
    }


    private fun verifyOtp()
    {
        progressDialog = ProgressD.show(
            this@OtpActivity,
            resources.getString(R.string.login),
            true,
            false,
            null
        )
        val credential = verificationid?.let { finalOtp?.let { it1 ->
            PhoneAuthProvider.getCredential(it, it1)
        } }
        credential?.let { signInWithCredential(it) }
    }


    private fun signInWithCredential(credential: PhoneAuthCredential) {
        mAuth?.signInWithCredential(credential)
            ?.addOnCompleteListener(OnCompleteListener<AuthResult?> { task ->
                if (task.isSuccessful)
                {
                    val i = Intent(this@OtpActivity, HomeActivity::class.java)
                    startActivity(i)
                    finish()
                    finalOtp?.let { sessionManager.createTokenSession(it) }
                    progressDialog!!.dismiss()
                    Toast.makeText(this@OtpActivity, getString(R.string.otpverified), Toast.LENGTH_LONG).show()
                }
                else {
                    Toast.makeText(this@OtpActivity, task.exception!!.message, Toast.LENGTH_LONG)
                        .show()
                    progressDialog!!.dismiss()
                }
            })
    }
}
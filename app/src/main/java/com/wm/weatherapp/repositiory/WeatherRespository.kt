package com.wm.weatherapp.repositiory

import com.wm.weatherapp.model.WeatherResponse
import com.wm.weatherapp.network.RestApi
import com.wm.weatherapp.network.SafeApiRequest


class WeatherRespository(private val api: RestApi) : SafeApiRequest()
{
    suspend fun getWeatherapi(city: String): WeatherResponse
    {
        return apiRequest { api.getWeather(city) }
    }
}